angular.module('sportskaCentrala', [
  'ionic', 
  'sportskaCentrala.controllers', 
  'sportskaCentrala.services', 
  'ngStorage',
  'ngCordova'
])

.run(function($ionicPlatform, $rootScope, $cordovaNetwork, HttpGetServices, $localStorage, $rootScope, localStorage, $location) {
  $ionicPlatform.ready(function() {

    /*if(typeof analytics !== undefined) {
        analytics.startTrackerWithId("UA-1923220-6");
    } else {
        console.log("Google Analytics Unavailable");
    }*/

    //test
    // if(typeof analytics !== undefined) {
    //     analytics.startTrackerWithId("UA-89110449-1");
    // } else {
    //     console.log("Google Analytics Unavailable");
    // }

    if($localStorage.notifications != undefined) {
        $rootScope.notifications = $localStorage.notifications;
    } else {
        $localStorage.notifications = { checked: true };
        $rootScope.notifications = $localStorage.notifications;
    }

    var notificationOpenedCallback = function(result) {

        var data = result.notification.payload.additionalData;
        
        if (data && data.targetUrl) {
          HttpGetServices.getListData(HttpGetServices.endpoint)
          .then(function successCallback(response) {

              $localStorage.data = response;
              var localData = localStorage.getData().NewsPages;
          
              if(localData != undefined) {
                for(var i = 0; i < localData.length; i++) {
                  if(localData[i].id == data.targetUrl) {
                      $location.path('/app/vest/' + data.targetUrl);
                  }
                }
              }

            }, function errorCallback(response) {

            });
        }
    };

    // init OneSignal Notifications based on device Platform
    if(ionic.Platform.isAndroid()) {
        window.plugins.OneSignal
        .startInit("d1f2229d-719b-49b4-813a-6e76a846e97c", "106975338110")
        .inFocusDisplaying(window.plugins.OneSignal.OSInFocusDisplayOption.Notification)
        .handleNotificationOpened(notificationOpenedCallback)
        .endInit();
    } else {
        window.plugins.OneSignal
        .startInit("d1f2229d-719b-49b4-813a-6e76a846e97c", "")
        .inFocusDisplaying(window.plugins.OneSignal.OSInFocusDisplayOption.Notification)
        .handleNotificationOpened(notificationOpenedCallback)
        .endInit();
    }

    $rootScope.$watch(function () { return $localStorage.notifications.checked; },function(newVal,oldVal){
        if(newVal == true) {
            window.plugins.OneSignal.setSubscription(true);
        } else {
            window.plugins.OneSignal.setSubscription(false);
        }
    });

    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }

    if (window.StatusBar) {
      
      StatusBar.styleDefault();
    }

  });
})

.directive('externalscript', function () {
    var injectScript = function(element) {
        var instagramScript = angular.element(document.createElement('script'));
        instagramScript.attr('async defer');
        instagramScript.attr('charset', 'utf-8');
        instagramScript.attr('src', 'http://platform.instagram.com/en_US/embeds.js');
        instagramScript.attr('onload', 'window.instgrm.Embeds.process()');
        element.append(instagramScript);
        var twitterScript = angular.element(document.createElement('script'));
        twitterScript.attr('async defer');
        twitterScript.attr('charset', 'utf-8');
        twitterScript.attr('src', 'http://platform.twitter.com/widgets.js');
        element.append(twitterScript);
    };

    return {
        link: function(scope, element) {
            injectScript(element);
        }
    };
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.pocetna', {
    url: '/pocetna',
    views: {
      'menuContent': {
        templateUrl: 'templates/pocetna.html',
        controller: 'PocetnaCtrl'
      }
    }
  })

  .state('app.live', {
    cache: false,
    url: '/live',
    views: {
      'menuContent': {
        templateUrl: 'templates/live.html',
        controller: 'LiveCtrl'
      }
    }
  })

    .state('app.cat', {
      url: '/cat/:catId',
      params: {
          catId: null
      },
      views: {
        'menuContent': {
          templateUrl: 'templates/category.html',
          controller: 'CategoryCtrl'
        }
      }
    })
    .state('app.vest', {
      url: '/vest/:vestId',
      views: {
        'menuContent': {
          templateUrl: 'templates/vest.html',
          controller: 'VestCtrl'
        }
      }
    })
    .state('app.catvest', {
      url: '/catvest/:vestId',
      views: {
        'menuContent': {
          templateUrl: 'templates/vest.html',
          controller: 'CatVestCtrl'
        }
      }
    })
    .state('app.login', {
      url: '/login',
      views: {
        'menuContent': {
          templateUrl: 'templates/login.html',
          controller: 'LoginCtrl'
        }
      }
    });

  $urlRouterProvider.otherwise('/app/pocetna');
});
