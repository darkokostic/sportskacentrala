angular.module('sportskaCentrala.services', [])

.factory('userService', function($http, $localStorage) {
    
    function checkIfLoggedIn() {

        if($localStorage.token == 0 || $localStorage.token == undefined) {
            return false;
        }
        else {
            return true;
        }
    }

    function login(username, password, onSuccess, onError, HttpGetServices) {

        var params = {
            user: username,
            password: password
        };

        $http.post('http://sportskacentrala.com/skin/api/user.php?user='+ username +'&password='+ password, params, 
            {
                headers: {'Content-Type': 'application/json'}
            } 
        ).
        then(function(response) {
            if(response.data.uid != 0) {
                $localStorage.token = response.data.uid;
                $localStorage.liveUrl = response.data.liveurl;
                onSuccess(response.data);
            } else {

                onError(response.data);
            }

        }, function(response) {

            onError(response.data);

        });

    }

    function logout() {

        $localStorage.token = 0;

    }

    function getCurrentToken() {
        return $localStorage.token;
    }


    return {
        checkIfLoggedIn: checkIfLoggedIn,
        login: login,
        logout: logout,
        getCurrentToken: getCurrentToken
    }

})

.service('HttpGetServices', ['$http', function($http) {

    function getListData(url) {
        return $http.get(url)
            .then(function(response) {
                return response.data;
        })
    }

    function getSingleData(url, id) {
        
        var urlSingle = url + id;
        
        return $http.get(urlSingle)
            .then(function(response) {
                return response.data;
        })
    }

    function getLiveTVUrl() {
        return $http.get('http://www.sportskacentrala.com/skin/api/live.php')
            .then(function(response) {
                return response.data;
            })
    }

    return {
        endpoint: 'http://sportskacentrala.com/skin/api/news.php',
        getListData: getListData,
        getSingleData: getSingleData,
        getLiveTVUrl: getLiveTVUrl
    }

}])

.service('parseHtmlService', ['$sce', function($sce) {

    function parse(data) {
        return $sce.trustAsHtml(data);
    }

    return {
        parse: parse
    }

}])

.service('localStorage', function ($localStorage, HttpGetServices, $ionicPopup) {

    function getData() {
        return $localStorage.data;
    }

    function addData() {
        HttpGetServices.getListData(HttpGetServices.endpoint)
        .then(function successCallback(response) {

            $localStorage.data = response;

          }, function errorCallback(response) {

          });
    }
    
    function deleteData () {
      $localStorage.$reset({ data: undefined });
    }
    
    return {
        getData: getData,
        addData: addData,
        deleteData: deleteData
    };
})

.service ('AdsService', function ($ionicPlatform, $ionicPopup) {
    
    var admob_key;
    var admob = null;

    var setAds = function() {

        $ionicPlatform.ready(function() {

            if(window.plugins && window.plugins.AdMob) {
                //admob_key = device.platform == "Android" ? "ca-app-pub-5650202701399956/2908607022" : "ca-app-pub-5650202701399956/5862073423";
                admob = window.plugins.AdMob;
                admob.createBannerView( 
                    {
                        'publisherId': 'ca-app-pub-3709908666592437/1619561219',
                        'adSize': admob.AD_SIZE.BANNER,
                        'bannerAtTop': false
                    }, 
                    function() {
                        admob.requestAd(
                            { 'isTesting': false }, 
                            function() {
                                admob.showAd(true);
                            }, 
                            function() { }
                        );
                    }, 
                    function() { }
                );
                admob.createInterstitialView( 
                    {
                        'interstitialAdId': 'ca-app-pub-3709908666592437/3096294414',
                        'offsetTopBar': false,
                        'isTesting': false, 
                        'autoShow': true
                    }, 
                    function() {
                        admob.requestInterstitialAd(
                            {}, 
                            function() {
                            }, 
                            function() { }
                        );
                    }, 
                    function() { }
                );

            }

        })
    }

    var removeAds = function() {

        $ionicPlatform.ready(function() {

            if(window.plugins && window.plugins.AdMob) {
                admob = window.plugins.AdMob;
                admob.destroyBannerView();
            }
        })
    }
    
    return {
        setAds: setAds,
        removeAds: removeAds
    };
});