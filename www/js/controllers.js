angular.module('sportskaCentrala.controllers', [])

.controller('AppCtrl', function($scope, userService, $timeout, localStorage, $stateParams, $cordovaNetwork, $rootScope, $ionicPopup, HttpGetServices, $ionicLoading, $location, $ionicHistory, $ionicLoading, AdsService, $ionicPlatform, $state, $cordovaGoogleAnalytics) {

    /*if(typeof analytics !== undefined) { 
        analytics.trackView("Mobile App", '', true); 
    }*/

    $scope.pushNotificationChange = function() {
        $scope.notifications.checked;
    }

    $rootScope.loggedIn = false;

    if(userService.checkIfLoggedIn() == true) {
        $rootScope.loggedIn = true;
        AdsService.removeAds();
    } else {
        $rootScope.loggedIn = false;
        AdsService.setAds();
    }

    $scope.logout = function() {
        $ionicLoading.show({ template: 'Uspešno ste se odjavili!', noBackdrop: true, duration: 1000 });
        $rootScope.loggedIn = false;
        AdsService.setAds();
        userService.logout();
        $location.path('/');
    }

    document.addEventListener("deviceready", function () {
        $scope.facebookGo = function() {
            window.open('fb://page/38525242835', "_system", 'location=no');
        }

        $scope.twitterGo = function() {
            window.open('https://twitter.com/scentrala', "_system");
        }

        $scope.betGo = function() {
            window.open('https://sports.bwin.com/en/sports?wm=3450471&zoneId=1103023', "_system");
        }
        var alertPopup = null;

        if($cordovaNetwork.isOnline()) {

                HttpGetServices.getListData(HttpGetServices.endpoint)
                    .then(function successCallback(response) {

                        $scope.categories = response.Categories;

                        localStorage.addData();

                      }, function errorCallback(response) {


                      });

          } else if(localStorage.getData() != undefined) {
                
                $scope.categories = localStorage.getData().Categories;

          } 

    // listen for Online event
    $rootScope.$on('$cordovaNetwork:online', function(event, networkState) {

        HttpGetServices.getListData(HttpGetServices.endpoint)
            .then(function successCallback(response) {

                $scope.categories = response.Categories;
                localStorage.addData();

              }, function errorCallback(response) {


              });

    })

  }, false);

})

.controller('PocetnaCtrl', function($ionicPlatform, $scope, HttpGetServices, localStorage, $rootScope, $ionicLoading, $timeout, $cordovaNetwork, $ionicHistory, $ionicPopup) {

    var deviceWidth = window.screen.width;
    if(deviceWidth < 320) {
        $scope.char = 20;
        $scope.firstChar = 53;
    } else if(deviceWidth >= 320 && deviceWidth < 360) {
        $scope.char = 22;
        $scope.firstChar = 58;
    } else if(deviceWidth >= 360 && deviceWidth < 375) {
        $scope.char = 30;
        $scope.firstChar = 65;
    } else if(deviceWidth >= 375 && deviceWidth < 400) {
        $scope.char = 30;
        $scope.firstChar = 70;
    } else if(deviceWidth >= 400 && deviceWidth < 500) {
        $scope.char = 35;
        $scope.firstChar = 80;
    } else if(deviceWidth >= 500 && deviceWidth < 700) {
        $scope.char = 55;
        $scope.firstChar = 100;
    } else if(deviceWidth >= 700 && deviceWidth < 800) {
        $scope.char = 60;
        $scope.firstChar = 110;
    } else if(deviceWidth >= 800) {
        $scope.char = 65;
        $scope.firstChar = 120;
    }

    window.addEventListener("orientationchange", function(){
        var deviceWidth = window.screen.width;
        if(deviceWidth < 320) {
            $scope.char = 20;
            $scope.firstChar = 53;
        } else if(deviceWidth >= 320 && deviceWidth < 360) {
            $scope.char = 22;
            $scope.firstChar = 58;
        } else if(deviceWidth >= 360 && deviceWidth < 375) {
            $scope.char = 30;
            $scope.firstChar = 65;
        } else if(deviceWidth >= 375 && deviceWidth < 400) {
            $scope.char = 30;
            $scope.firstChar = 70;
        } else if(deviceWidth >= 400 && deviceWidth < 500) {
            $scope.char = 35;
            $scope.firstChar = 80;
        } else if(deviceWidth >= 500 && deviceWidth < 700) {
            $scope.char = 55;
            $scope.firstChar = 100;
        } else if(deviceWidth >= 700 && deviceWidth < 800) {
            $scope.char = 60;
            $scope.firstChar = 110;
        } else if(deviceWidth >= 800) {
            $scope.char = 65;
            $scope.firstChar = 120;
        }
    });

    $ionicHistory.clearHistory();

    var firstPost;
    var topPosts = [];
    var restPosts = [];
    var data;
    var alertPopup = null;
    var newsLength;

    $scope.numberOfItemsToDisplay = 10;

    $scope.sync = function() {
        HttpGetServices.getListData(HttpGetServices.endpoint)
            .then(function successCallback(response) {
                
                var firstPost;
                var topPosts = [];
                var restPosts = [];
                var data;

                data = response.NewsPages;

                // top post
                firstPost = data[0];

                // posts from 1 to 4
                if(data.length < 5) {
                    for(var i = 1; i < data.length; i++) {
                        topPosts.push(data[i]);
                    }
                } else {
                    for(var i = 1; i < 5; i++) {
                        topPosts.push(data[i]);
                    }
                }

                // rest posts
                for(var i = 5; i < data.length; i++) {
                    restPosts.push(data[i]);
                }

                newsLength = restPosts.length;
                $scope.firstPost = firstPost;
                $scope.topPosts = topPosts;
                $scope.news = restPosts;
                localStorage.addData();

          }, function errorCallback(response) {

          })
         .finally(function() {
           // Stop the ion-refresher from spinning
           $scope.$broadcast('scroll.refreshComplete');
        });
    };

    document.addEventListener("deviceready", function () {
        var alertPopup = null;

        if($cordovaNetwork.isOnline()) {
                
                $ionicLoading.show({
                    content: 'Loading',
                    animation: 'fade-in',
                    showBackdrop: true,
                    maxWidth: 200,
                    showDelay: 0
                });


                HttpGetServices.getListData(HttpGetServices.endpoint)
                    .then(function successCallback(response) {

                        var firstPost;
                        var topPosts = [];
                        var restPosts = [];
                        var data;

                        data = response.NewsPages;
                        
                        // top post
                        firstPost = data[0];

                        // posts from 1 to 4
                        if(data.length < 5) {
                            for(var i = 1; i < data.length; i++) {
                                topPosts.push(data[i]);
                            }
                        } else {
                            for(var i = 1; i < 5; i++) {
                                topPosts.push(data[i]);
                            }
                        }

                        // rest posts
                        for(var i = 5; i < data.length; i++) {
                            restPosts.push(data[i]);
                        }

                        newsLength = restPosts.length;
                        $scope.firstPost = firstPost;
                        $scope.topPosts = topPosts;
                        $scope.news = restPosts;
                        localStorage.addData();

                        if($scope.firstPost != undefined && $scope.topPosts != undefined && $scope.news != undefined) {
                            $ionicLoading.hide();
                        }

                      }, function errorCallback(response) {

                      });
          } else if(localStorage.getData() != undefined) {

                var firstPost;
                var topPosts = [];
                var restPosts = [];
                var data;
                
                data = localStorage.getData().NewsPages;
                
                // top post
                firstPost = data[0];

                // posts from 1 to 4
                for(var i = 1; i < 5; i++) {
                    topPosts.push(data[i]);
                }

                // rest posts
                for(var i = 5; i < data.length; i++) {
                    restPosts.push(data[i]);
                }

                newsLength = restPosts.length;
                $scope.firstPost = firstPost;
                $scope.topPosts = topPosts;
                $scope.news = restPosts;
          } else {
               function networkAlert() {
                    alertPopup = $ionicPopup.alert({
                        title: 'Internet konekcija',
                        template: 'Nemate internet konekciju na vašem uređaju.'
                    });
                   
                    alertPopup.then(function(res) {
                        if(res) {
                            if($cordovaNetwork.isOnline() == false) {
                                networkAlert();
                            } else {
                                alertPopup.close();
                            }
                        }
                    });
               };
               networkAlert();
          }

    // listen for Online event
    $rootScope.$on('$cordovaNetwork:online', function(event, networkState) {
        if(alertPopup != null) {
            alertPopup.close();
        }
        $ionicLoading.show({
            content: 'Loading',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
          });

        HttpGetServices.getListData(HttpGetServices.endpoint)
            .then(function successCallback(response) {
               
                var firstPost;
                var topPosts = [];
                var restPosts = [];
                var data;

                data = response.NewsPages;  

                // top post
                firstPost = data[0];

                // posts from 1 to 4
                for(var i = 1; i < 5; i++) {
                    topPosts.push(data[i]);
                }

                // rest posts
                for(var i = 5; i < data.length; i++) {
                    restPosts.push(data[i]);
                }

                newsLength = restPosts.length;
                $scope.firstPost = firstPost;
                $scope.topPosts = topPosts;
                $scope.news = restPosts;
                localStorage.addData();
                if($scope.firstPost != undefined && $scope.topPosts != undefined && $scope.news != undefined) {
                    $rootScope.$scope = "false";
                    $ionicLoading.hide();
                }

              }, function errorCallback(response) {

              });

    })

  }, false);

    $scope.loadMore = function(done) {

        if(newsLength <= $scope.numberOfItemsToDisplay) {
            $scope.$broadcast('scroll.infiniteScrollComplete');
        } else {
            $timeout(function () {
                if($scope.numberOfItemsToDisplay == 30) {
                    $scope.numberOfItemsToDisplay += 5;
                } else {
                    $scope.numberOfItemsToDisplay += 10;
                }
                $scope.$broadcast('scroll.infiniteScrollComplete');
            }, 2000);
        }        
    }
    
})

.controller('CategoryCtrl', function($scope, $stateParams, localStorage, $rootScope, HttpGetServices, $timeout, $cordovaNetwork) {
    
    var deviceWidth = window.screen.width;
    if(deviceWidth < 320) {
        $scope.char = 20;
        $scope.firstChar = 53;
    } else if(deviceWidth >= 320 && deviceWidth < 360) {
        $scope.char = 22;
        $scope.firstChar = 58;
    } else if(deviceWidth >= 360 && deviceWidth < 375) {
        $scope.char = 30;
        $scope.firstChar = 65;
    } else if(deviceWidth >= 375 && deviceWidth < 400) {
        $scope.char = 30;
        $scope.firstChar = 70;
    } else if(deviceWidth >= 400 && deviceWidth < 500) {
        $scope.char = 35;
        $scope.firstChar = 80;
    } else if(deviceWidth >= 500 && deviceWidth < 700) {
        $scope.char = 55;
        $scope.firstChar = 100;
    } else if(deviceWidth >= 700 && deviceWidth < 800) {
        $scope.char = 60;
        $scope.firstChar = 110;
    } else if(deviceWidth >= 800) {
        $scope.char = 65;
        $scope.firstChar = 120;
    }

    window.addEventListener("orientationchange", function(){
        var deviceWidth = window.screen.width;
        if(deviceWidth < 320) {
            $scope.char = 20;
            $scope.firstChar = 55;
        } else if(deviceWidth >= 320 && deviceWidth < 360) {
            $scope.char = 22;
            $scope.firstChar = 60;
        } else if(deviceWidth >= 360 && deviceWidth < 375) {
            $scope.char = 30;
            $scope.firstChar = 65;
        } else if(deviceWidth >= 375 && deviceWidth < 400) {
            $scope.char = 30;
            $scope.firstChar = 70;
        } else if(deviceWidth >= 400 && deviceWidth < 500) {
            $scope.char = 35;
            $scope.firstChar = 80;
        } else if(deviceWidth >= 500 && deviceWidth < 700) {
            $scope.char = 55;
            $scope.firstChar = 100;
        } else if(deviceWidth >= 700 && deviceWidth < 800) {
            $scope.char = 60;
            $scope.firstChar = 110;
        } else if(deviceWidth >= 800) {
            $scope.char = 65;
            $scope.firstChar = 120;
        }
    });

    $scope.message = "true";
    $scope.numberOfItemsToDisplay = 10;
    var newsLength;
    var id = $stateParams.catId;
    var data;
    var categories;

    $scope.sync = function() {
        HttpGetServices.getListData(HttpGetServices.endpoint)
            .then(function successCallback(response) {

                var id = $stateParams.catId;
                var data = response.NewsPages;
                var categories = response.Categories;

                var newsCat = [];

                
                for(var i = 0; i < categories.length; i++) {
                    if(categories[i].id == id) {
                        $scope.title = categories[i].category;
                    }
                }

                for(var i = 0; i < data.length; i++) {
                    if(data[i].cid == id) {
                        newsCat.push(data[i]);
                    }
                }

                var firstPostCat;
                var topPostsCat = [];
                var restPostsCat = [];

                // top post
                firstPostCat = newsCat[0];

                // posts from 1 to 4
                if(newsCat.length < 5) {
                    for(var i = 1; i < newsCat.length; i++) {
                        topPostsCat.push(newsCat[i]);
                    }
                } else {
                    for(var i = 1; i < 5; i++) {
                        topPostsCat.push(newsCat[i]);
                    }
                }

                // rest posts
                for(var i = 5; i < newsCat.length; i++) {
                    restPostsCat.push(newsCat[i]);
                }
                if(firstPostCat == undefined) {
                    $scope.empty = "true";
                    //$scope.noNews = "false";
                } else {
                    newsLength = restPostsCat.length;
                    $scope.newsCat = restPostsCat;
                    $scope.topPostsCat = topPostsCat;
                    $scope.firstPostCat = firstPostCat;
                }
                localStorage.addData();

          }, function errorCallback(response) {

          })
         .finally(function() {
           // Stop the ion-refresher from spinning
           $scope.$broadcast('scroll.refreshComplete');
        });
    };

    if(localStorage.getData() != undefined) {
            
            data = localStorage.getData().NewsPages;
            categories = localStorage.getData().Categories;
    
    } else {

        HttpGetServices.getListData(HttpGetServices.endpoint)
            .then(function successCallback(response) {

                data = response.NewsPages;
                categories = response.Categories;

              }, function errorCallback(response) {

              });
    } 

    var newsCat = [];

    
    for(var i = 0; i < categories.length; i++) {
        if(categories[i].id == id) {
            $scope.title = categories[i].category;
        }
    }

    for(var i = 0; i < data.length; i++) {
        if(data[i].cid == id) {
            newsCat.push(data[i]);
        }
    }

    var firstPostCat;
    var topPostsCat = [];
    var restPostsCat = [];

    // top post
    firstPostCat = newsCat[0];

    // posts from 1 to 4
    if(newsCat.length < 5) {
        for(var i = 1; i < newsCat.length; i++) {
            topPostsCat.push(newsCat[i]);
        }
    } else {
        for(var i = 1; i < 5; i++) {
            topPostsCat.push(newsCat[i]);
        }
    }

    // rest posts
    for(var i = 5; i < newsCat.length; i++) {
        restPostsCat.push(newsCat[i]);
    }
    if(firstPostCat == undefined) {
        $scope.empty = "true";
        //$scope.noNews = "false";
    } else {
        newsLength = restPostsCat.length;
        $scope.newsCat = restPostsCat;
        $scope.topPostsCat = topPostsCat;
        $scope.firstPostCat = firstPostCat;
    }

    $scope.loadMore = function(done) {
        if(newsLength <= $scope.numberOfItemsToDisplay) {
            $scope.$broadcast('scroll.infiniteScrollComplete');
        } else {
            $timeout(function () {
                $scope.numberOfItemsToDisplay += 10;
                $scope.$broadcast('scroll.infiniteScrollComplete');
            }, 2000);
        }        
    }

})

.controller('VestCtrl', function($scope, $stateParams, localStorage, $rootScope, parseHtmlService, $cordovaSocialSharing) {
    
    var id = $stateParams.vestId;
    var data = localStorage.getData().NewsPages;
    var categories = localStorage.getData().Categories;
    var catId;
    var indexOfVest;

    for(var i = 0; i < data.length; i++) {

        if(data[i].id == id) {
            $scope.vest = data[i];
            indexOfVest = i;
            catId = data[i].cid;
        }
    }

    for(var i = 0; i < categories.length; i++) {
        if(categories[i].id == catId) {
            $scope.title = categories[i].category;
        }
    }

    $scope.left = function() {

        indexOfVest++;
        if(indexOfVest == data.length) {
            indexOfVest = (data.length-1);
        }
        for(var i = 0; i < data.length; i++) {
            if(i == indexOfVest) {
                $scope.vest = data[i];
            }
        }
    }
    $scope.right = function() {

        indexOfVest--;
        if(indexOfVest < 0) {
            indexOfVest = 0;
        }

        for(var i = 0; i < data.length; i++) {
            if(i == indexOfVest) {
                $scope.vest = data[i];
            }
        }
    }

    $scope.parseHtml = function(data) {
        return parseHtmlService.parse(data);
    };

    $scope.share = function() {
        $cordovaSocialSharing
        .share(null, null, null, $scope.vest.link)
        .then(function(result) {
          // Success!
        }, function(err) {
          // An error occured. Show a message to the user
        });
    }

})

.controller('CatVestCtrl', function($scope, $stateParams, localStorage, $rootScope, parseHtmlService) {
    
    var id = $stateParams.vestId;
    var data = localStorage.getData().NewsPages;
    var categories = localStorage.getData().Categories;
    var catId;
    var indexOfVest;
    var newsFromCat = [];


    for(var i = 0; i < data.length; i++) {

        if(data[i].id == id) {
            $scope.vest = data[i];
            catId = data[i].cid;
        }
    }

    for(var i = 0; i < data.length; i++) {
        if(data[i].cid == catId) {
            newsFromCat.push(data[i]);
        }
    }

    for(var i = 0; i < newsFromCat.length; i++) {

        if(newsFromCat[i].id == id) {
            indexOfVest = i;
        }
    }

    $scope.left = function() {

        indexOfVest++;
        if(indexOfVest == newsFromCat.length) {
            indexOfVest = (newsFromCat.length-1);
        }
        for(var i = 0; i < newsFromCat.length; i++) {
            if(i == indexOfVest) {
                $scope.vest = newsFromCat[i];
            }
        }
    }
    $scope.right = function() {

        indexOfVest--;
        if(indexOfVest < 0) {
            indexOfVest = 0;
        }

        for(var i = 0; i < newsFromCat.length; i++) {
            if(i == indexOfVest) {
                $scope.vest = newsFromCat[i];
            }
        }
    }

    $scope.parseHtml = function(data) {
        return parseHtmlService.parse(data);
    };
})

.controller('LoginCtrl', function($scope, AdsService, $stateParams, $rootScope, $location, $ionicHistory, userService, $ionicLoading) {
    
    $scope.login = function(username, password) {
        if(username != undefined && username != '' && password != undefined && password != '') {
            userService.login(username, password, 
            function(response) {
                    $ionicLoading.show({ template: 'Uspešno ste se ulogovali!', noBackdrop: true, duration: 1000 });
                    $rootScope.loggedIn = true;
                    AdsService.removeAds();
                    $location.path('/');
                    $ionicHistory.nextViewOptions({
                       disableBack: true
                    });
                },
                function(response) {
                    console.log(response);
                    if(response == null) {
                        $ionicLoading.show({ template: 'Nemate internet konekciju!', noBackdrop: true, duration: 1000 });

                    } else {
                        $ionicLoading.show({ template: 'Pogrešno korisničko ime ili lozinka!', noBackdrop: true, duration: 1000 });
                    }
                }
            );
        } else {
            $ionicLoading.show({ template: 'Unesite korisničko ime ili lozinku!', noBackdrop: true, duration: 1000 });
        }
    }
})

.controller('LiveCtrl', function($scope, $sce, HttpGetServices) {

    $scope.trustSrc = function(src) {
        return $sce.trustAsResourceUrl(src);
    }

    HttpGetServices.getLiveTVUrl()
        .then(function (response) {
            console.log(response);
            $scope.liveurl = response.liveurl
        })
});